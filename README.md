# Treasure hunt

Race between several players *(hunters)* towards a treasure.

GUI Game *(POA - L2, S4)*.

## How to use it

**First solution:**
 - Download/Clone the repository
 - Launch the jar archive (`java -jar chasse_tresor.jar`)

**Second solution:**
 - Download the release
 - Launch the jar archive (`java -jar treasure_hunt.jar`)

## Features

 - Run a game with a random board
 - Buuild your own board