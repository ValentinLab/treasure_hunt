package model;

/**
 * Interface permettant à un joueur d'interagir avec une case du plateau
 *
 * @author Université de Franche-Comté
 */
public interface Questionnable {
	public String process(Hunter h);
}
